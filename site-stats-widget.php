<?php
/*
Plugin Name: Site Stats Widget
Plugin URI: https://xwp.co/
Description: Present site statistics on the frontend.
Version: 0.1
Author: Allan Collins
Author URI: https://www.allancollins.net/
License: GPL2
*/
define( 'SSW_PATH', plugin_dir_path( __FILE__ ) . '/' );

require_once 'vendor/autoload.php';

// Register the widget:
add_action( 'widgets_init', function () {
	register_widget( 'Site_Stats_Widget' );
} );

// Register the custom endpoints
$site_stats_endpoints = new Site_Stats_Endpoints();
$site_stats_endpoints->hooks();

// Enqueue Scripts/Stylesheets:
add_action( 'wp_enqueue_scripts', function () {
	wp_enqueue_script( 'site-stats', plugin_dir_url( __FILE__ ) . '/assets/js/script.min.js', array( 'jquery' ), '0.1.0', true );
	wp_localize_script( 'site-stats', 'site_stats', array(
		'rest_url'  => esc_url( get_rest_url( null, 'site-stats/v1' ) ),
		'multisite' => is_multisite()
	) );
} );

