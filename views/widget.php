<?php
echo $args['before_widget'];
if ( ! empty( $instance['title'] ) ) {
	echo $args['before_title'] . apply_filters( 'widget_title', $instance['title'] ) . $args['after_title'];
}
?>
    <form id="ssw_sites_form">
        <select id="ssw_sites">

        </select>
    </form>
    <br>
    <strong>Total Posts By Post Type</strong><br>
    <ul id="ssw_stats">

    </ul>
    <br>
    <strong>Total Users</strong><br>
    <ul id="ssw_stats_users">

    </ul>
<?php echo $args['after_widget'];