/*global site_stats, jQuery, setTimeout, clearTimeout */
(function ($) {
    var current_blog_id = '';
    var ssw_timer;

    var init = function () {
        if (site_stats.multisite) {
            get_sites();
        } else {
            $('#ssw_sites_form').hide();
        }
    };

    var get_sites = function () {
        $.get(site_stats.rest_url + '/getsites', function (res) {
            $.each(res, function (index, site) {
                $('#ssw_sites').append('<option value="' + site.blog_id + '">' + site.site_title + '</option>');
            });

        });
        $('#ssw_sites').on('change', function () {
            clearTimeout(ssw_timer);
            current_blog_id = $(this).val();
            get_stats();
        });
    };

    var get_stats = function () {

        var rest_url = site_stats.rest_url + '/get';
        if ('' !== current_blog_id) {
            rest_url += '/' + current_blog_id;
        }
        $.get(rest_url, function (res) {
            $('#ssw_stats,#ssw_stats_users').empty();

            $.each(res.post_count, function (index, post_count) {
                $('#ssw_stats').append('<li>' + index + ': ' + post_count + '</li>');
            });
            $('#ssw_stats_users').append('<li>Users: ' + res.user_count.total_users + '</li>');
        });

        ssw_timer = setTimeout(get_stats, 60000);
    };

    $.when($.ready).then(function () {
        init();
        get_stats();
    });
})(jQuery);