<?php

class Site_Stats_Endpoints {

	/**
	 * Site_Stats_Endpoints constructor.
	 */
	public function __construct() {
	}

	/**
	 * WordPress hooks.
	 */
	public function hooks() {
		add_action( 'rest_api_init', array( $this, 'register_endpoints' ) );
	}

	/**
	 * Register REST API endpoints.
	 */
	public function register_endpoints() {

		register_rest_route( 'site-stats/v1', '/get', array(
			'methods'  => 'GET',
			'callback' => array( $this, 'get_stats' ),
		) );

		if ( is_multisite() ) {
			register_rest_route( 'site-stats/v1', '/getsites', array(
				'methods'  => 'GET',
				'callback' => array( $this, 'get_sites' ),
			) );

			register_rest_route( 'site-stats/v1', '/get/(?P<blog_id>\d+)', array(
				'methods'  => 'GET',
				'callback' => array( $this, 'get_stats_ms' ),
			) );
		}
	}

	/**
	 * Custom REST API endpoint that returns list of sites on multisite.
	 *
	 * @param WP_REST_Request $request
	 *
	 * @return array
	 */
	public function get_sites( WP_REST_Request $request ) {

		$site_list = get_transient( 'site-stats-getsites' );

		if ( false === $site_list ) {

			$sites     = get_sites();
			$site_list = array();
			foreach ( $sites as $site ):
				switch_to_blog( $site->blog_id );
				$site_list[] = array(
					'site_title' => get_option( 'blogname' ),
					'blog_id'    => $site->blog_id
				);
				restore_current_blog();
			endforeach;
			set_transient( 'site-stats-getsites', $site_list, 300 );

		}

		return $site_list;
	}

	/**
	 * Custom REST API endpoint for single site installs.
	 *
	 * @param WP_REST_Request $request
	 *
	 * @return array
	 */
	public function get_stats( WP_REST_Request $request ) {
		return $this->fetch_stats();
	}

	/**
	 * Custom REST API endpoint for multisite installs.
	 *
	 * @param WP_REST_Request $request
	 *
	 * @return array
	 */
	public function get_stats_ms( WP_REST_Request $request ) {

		$blog_id = ( isset( $request['blog_id'] ) ) ? absint( $request['blog_id'] ) : 1;

		switch_to_blog( $blog_id );
		$stats = $this->fetch_stats();
		restore_current_blog();

		return $stats;
	}

	/**
	 * Returns an array of site statistical information.
	 * @return array
	 */
	private function fetch_stats() {
		return array(
			'site_title' => get_option( 'blogname' ),
			'post_count' => $this->fetch_post_count(),
			'user_count' => $this->fetch_user_count()
		);
	}

	/**
	 * Returns an array of published post counts based upon post type.
	 * @return array
	 */
	private function fetch_post_count() {
		$post_types = get_post_types();
		$post_count = array();
		foreach ( $post_types as $post_type ):
			$the_count = wp_count_posts( $post_type, 'readable' );
			if ( 0 < $the_count->publish ) {
				$post_count[ $post_type ] = $the_count->publish;
			}
		endforeach;

		return $post_count;
	}

	/**
	 * Returns array of user role count.
	 * Note: count_users() has the potential to be resource intensive, therefore it is cached here.
	 * @return array
	 */
	private function fetch_user_count() {
		$user_count = get_transient( 'site-stats-users' );
		if ( false === $user_count ) {
			$user_count = count_users();
			set_transient( 'site-stats-users', $user_count, 300 );
		}

		return $user_count;
	}
}